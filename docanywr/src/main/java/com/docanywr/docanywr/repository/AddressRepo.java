package com.docanywr.docanywr.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.docanywr.docanywr.model.Address;

public interface AddressRepo extends JpaRepository<Address, Serializable> {
	
	
	
	Address findById(Long id);
	
	

}
