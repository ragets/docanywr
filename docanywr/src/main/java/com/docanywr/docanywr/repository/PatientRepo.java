package com.docanywr.docanywr.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.docanywr.docanywr.model.Patient;

public interface PatientRepo extends JpaRepository<Patient, Serializable>{
	
	Patient findById(Long id);

}
