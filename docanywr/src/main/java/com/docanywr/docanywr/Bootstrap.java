package com.docanywr.docanywr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.docanywr.docanywr.model.Address;
import com.docanywr.docanywr.model.Patient;
import com.docanywr.docanywr.repository.AddressRepo;
import com.docanywr.docanywr.repository.PatientRepo;
import com.docanywr.docanywr.service.PatientService;

@Component
public class Bootstrap  implements ApplicationListener<ContextRefreshedEvent>{
	
	
	@Autowired
	PatientService patientService;
	
	@Autowired
	PatientRepo patientRepo;
	
	@Autowired
	AddressRepo addressRepo;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		// TODO Auto-generated method stub
		if(patientService.getAllPatient().isEmpty()) {
			createPatient();
			
		}
		
	}
	
    public void  createPatient(){
    	 Patient patient = new Patient();
    	 patient.setFirstName("Dev");
    	 patient.setLastName("sharma");
    	 patient.setContactNumber("8877887788");
    	 patientRepo.save(patient);
    	 
    	 Address address = new Address();
    	 address.setAddressLine1("H. no. 50,Teachers Colony");
    	 address.setAddressLine2("Jahangirabad");
    	 address.setCountry("India");
    	 address.setCity("bulandshahr");
    	 address.setPatient(patient);
    	 addressRepo.save(address);
    	 
    	 address = new Address();
    	 address.setAddressLine1("H. no. 54,Teachers Colony");
    	 address.setAddressLine2("Jahangirabad");
    	 address.setCountry("India");
    	 address.setCity("bulandshahr");
    	 address.setPatient(patient);
    	 addressRepo.save(address);
    	 
    	 patient = new Patient();
    	 patient.setFirstName("Pavan");
    	 patient.setLastName("sharma");
    	 patient.setContactNumber("8877997788");
    	 patientRepo.save(patient);
    	 
    	 address = new Address();
    	 address.setAddressLine1("H. no. 86,Teachers Colony");
    	 address.setAddressLine2("Jahangirabad");
    	 address.setCountry("India");
    	 address.setCity("bulandshahr");
    	 address.setPatient(patient);
    	 addressRepo.save(address);
    	 
     }

}
