package com.docanywr.docanywr.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

@Configuration
public class FirebaseConfig {

	private static final Logger logger = LoggerFactory.getLogger(FirebaseConfig.class);

	@PostConstruct
	public void init() {

		logger.info("**************** FIREBASE CONFIGURATION ******************");

		FileInputStream serviceAccount = null;
		try {
			serviceAccount = new FileInputStream(new File("src/main/resources/static/serviceAccountKey.json"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		FirebaseOptions options = null;
		;
		try {
			options = new FirebaseOptions.Builder().setCredentials(GoogleCredentials.fromStream(serviceAccount))
					.setDatabaseUrl("https://docanywr.firebaseio.com").build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (FirebaseApp.getApps().isEmpty()) { // <--- check with this line
			FirebaseApp.initializeApp(options);
		}

	}

}
