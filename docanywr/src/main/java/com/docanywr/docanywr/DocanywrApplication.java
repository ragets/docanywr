package com.docanywr.docanywr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class DocanywrApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocanywrApplication.class, args);
	}

}
