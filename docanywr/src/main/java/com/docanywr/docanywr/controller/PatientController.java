package com.docanywr.docanywr.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.docanywr.docanywr.DTOS.PatientDTO;
import com.docanywr.docanywr.model.Patient;
import com.docanywr.docanywr.service.PatientService;
import com.docanywr.docanywr.util.ResponseUtil;
import com.google.firebase.auth.FirebaseAuthException;

@RestController
public class PatientController {
	
	
	private static final Logger logger = LoggerFactory.getLogger(PatientController.class);

	@Autowired
	PatientService patientService;

	@GetMapping("/getAllPatient")
	public ResponseEntity<?> getAllPatient() throws FirebaseAuthException {
		List<Patient> patients = patientService.getAllPatient();
		return ResponseUtil.createSuccessResponse(patients, "patient fetch Successfully");
	}

	@PutMapping("/patient")
	public ResponseEntity<?> updatePatient(@RequestBody PatientDTO dto) {
		Patient patient = patientService.updatePatient(dto);
		if (patient != null) {
			return ResponseUtil.createSuccessResponse(patient, "patient update Successfully");
		}
		return ResponseUtil.createErrorResponse("Patient no update , please try after some time");
	}

	@PostMapping("/patient")
	public ResponseEntity<?> addPatient(@RequestBody PatientDTO patient) {
		return ResponseUtil.createSuccessResponse(patientService.addPatient(patient), "patient added Successfully");
	}

	@PutMapping("/deletePatient")
	public ResponseEntity<?> deletePatient(@RequestBody PatientDTO dto) {
		patientService.deletePatient(dto.getId());
		return ResponseUtil.createSuccessResponse(dto, "patient removed successfully");
	}

}
