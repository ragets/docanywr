package com.docanywr.docanywr.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseUtil {
	
	public static ResponseEntity<Object> createSuccessResponse(Object response, String message) {
		Map<Object, Object> result = new HashMap<>();
		result.put("isSuccess", true);
		result.put("msg", message);
		result.put("data", response);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	public static ResponseEntity<Object> createErrorResponse(String msg, HttpStatus status) {
		Map<Object, Object> result = new HashMap<>();
		result.put("isSuccess", false);
		result.put("msg", msg);
		return new ResponseEntity<>(result, status);
	}

	
	
	public static ResponseEntity<Object> createErrorResponse(String msg) {
		Map<Object, Object> result = new HashMap<>();
		result.put("isSuccess", false);
		result.put("msg", msg);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

}
