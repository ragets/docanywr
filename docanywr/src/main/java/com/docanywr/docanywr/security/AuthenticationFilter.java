package com.docanywr.docanywr.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.GenericFilterBean;

import com.docanywr.docanywr.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;

public class AuthenticationFilter extends GenericFilterBean {

	private static final String AUTH_TOKEN_HEADER = "X-Xsrf-Token";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String authenticationToken = null;
		FirebaseToken token = null;
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		authenticationToken = httpServletRequest.getHeader(AUTH_TOKEN_HEADER);
		if (authenticationToken != null && !authenticationToken.isEmpty()) {
			try {
				token = FirebaseAuth.getInstance().verifyIdToken(authenticationToken);
			} catch (FirebaseAuthException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (token != null) {
				User user = new User();
				user.setUid(token.getUid());
				user.setName(token.getName());
				user.setEmailid(token.getEmail());
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user,
						token, null);
				authentication
						.setDetails(new WebAuthenticationDetailsSource().buildDetails((HttpServletRequest) request));
				SecurityContextHolder.getContext().setAuthentication(authentication);
			} else {
				HttpServletResponse httpServletResponse = (HttpServletResponse) response;
				httpServletResponse.setContentType("application/json");
				httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			}

		} else {
			HttpServletResponse httpServletResponse = (HttpServletResponse) response;
			httpServletResponse.setContentType("application/json");
			httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}

		chain.doFilter(request, response);

	}

}
