package com.docanywr.docanywr.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.docanywr.docanywr.DTOS.PatientDTO;
import com.docanywr.docanywr.model.Address;
import com.docanywr.docanywr.model.Patient;
import com.docanywr.docanywr.repository.AddressRepo;
import com.docanywr.docanywr.repository.PatientRepo;

@Service
@Transactional
public class PatientService {

	@Autowired
	PatientRepo patientRepo;
	
	@Autowired
	AddressService addressService;

	public List<Patient> getAllPatient() {
		return patientRepo.findAll();
	}

	public Patient updatePatient(PatientDTO dto) {
		Patient patient = patientRepo.findById(dto.getId());
		patient.setFirstName(dto.getFirstName());
		patient.setLastName(dto.getLastName());
		patient.setContactNumber(dto.getContactNumber());
		addressService.updateAddress(dto.getAddress());
		return patient;
	}

	public Patient addPatient(PatientDTO dto) {
		Patient patient = new Patient();
		patient.setFirstName(dto.getFirstName());
		patient.setLastName(dto.getLastName());
		patient.setContactNumber(dto.getContactNumber());
		patientRepo.save(patient);
		patient.setAddress(addressService.saveAddress(dto.getAddress(), patient));
		return patient;
	}

	public void deletePatient(Long id) {
		patientRepo.delete(patientRepo.findById(id));
	}
}
