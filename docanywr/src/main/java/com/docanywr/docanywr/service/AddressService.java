package com.docanywr.docanywr.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.docanywr.docanywr.model.Address;
import com.docanywr.docanywr.model.Patient;
import com.docanywr.docanywr.repository.AddressRepo;

@Service
@Transactional
public class AddressService {
	
	
	@Autowired
	AddressRepo addressRepo;
	
	
     public void updateAddress(List<Address> addresses) {
    	 Address address = null;
    	 for(Address add : addresses) {
    		 address = addressRepo.findById(add.getId());
    		 address.setAddressLine1(add.getAddressLine1());
    		 address.setAddressLine2(add.getAddressLine2());
    		 address.setCity(add.getCity());
    		 address.setCountry(add.getCountry());
    	 }
     }
     
	public List<Address>  saveAddress(List<Address> addresses,Patient patient) {
		Address address = null;
		List<Address> addressList = new ArrayList<Address>();
		for (Address add : addresses) {
			address = new Address();
			address.setAddressLine1(add.getAddressLine1());
			address.setAddressLine2(add.getAddressLine2());
			address.setCity(add.getCity());
			address.setCountry(add.getCountry());
			address.setPatient(patient);
			addressList.add(addressRepo.save(address));
		}
		return addressList;
	}

}
